import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;


public class App 
{
	private static final Logger logger = Logger.getLogger(App.class.getName());
    public static void main( String[] args )
    {
    	logger.info("TESTING JAVA JDK 11 STRING FEATURES\n");
    	TestStringFeatures testStringFeatures=new TestStringFeatures();
    	testStringFeatures.testingIsBlank();
    	logger.info("ALL JDK 11 STRING FEATURES ARE WORKING\n");
    	TestPatternRecognizingFeatures testPatternRecognizingFeatures = new TestPatternRecognizingFeatures();
    	testPatternRecognizingFeatures.testingAsMatchPredicate();
    	logger.info("ALL JAVA JDK 11 PATTERN RECOGNIZING FEATUERS ARE WORKING");
    	TestTimeUnitFeatures testTimeUnitFeatures = new TestTimeUnitFeatures();
    	testTimeUnitFeatures.testingConvert();
    	logger.info("ALL JAVA JDK 11 TIME UNIT FEATUERS ARE WORKING");
    	TestOptionalFeatures testOptionalFeatures= new TestOptionalFeatures();
    	testOptionalFeatures.testingIsEmpty();
    	logger.info("ALL JAVA JDK 11 OPTIONAL CLASS FEATUERS ARE WORKING");
    	TestNestedClassFeatures testNestedClassFeatures = new TestNestedClassFeatures();
    	TestNestedClassFeatures.A inner = testNestedClassFeatures.new A();
    	inner.printName();
    	logger.info("ALL JAVA JDK 11 NESTED CLASS FEATUERS ARE WORKING");
    	TestCollectionApiFeatures testCollectionApiFeatures = new TestCollectionApiFeatures();
    	testCollectionApiFeatures.testingIsArray();
    	logger.info("ALL JAVA JDK 11 COLLECTION FEATUERS ARE WORKING");
//    	TestHttpClientFeature testHttpClientFeature = new TestHttpClientFeature();
//    	testHttpClientFeature.testingHttpClient();
//    	logger.info("ALL JAVA JDK 11 HTTPCLIENT API FEATUERS ARE WORKING");
    	
    	
        }
}
class TestStringFeatures{
	static TestStringFeatures testStringFeatures = new TestStringFeatures();
	
	//This instance method returns a boolean value. Empty Strings and Strings with only white spaces are treated as blank.
	public void testingIsBlank() {
		System.out.println(" ".isBlank()); //true
        String s = "sarath";
        System.out.println(s.isBlank()); //false
        String s1 = "";
        System.out.println(s1.isBlank()); //true
        System.out.println("====================================================");
        testStringFeatures.testingLines();
        
	}
	
	//This method returns a stream of strings, which is a collection of all substrings split by lines
	public void testingLines() {
		String str = "legato\nhealth\ntechnologies"; 
        System.out.println(str);
        System.out.println(str.lines().collect(Collectors.toList()));
        System.out.println("====================================================");
        testStringFeatures.testingStrip();
	}
	
	//Removes the white space from both, beginning and the end of string.
	public void testingStrip() {
		String str = "   Anthem   "; 
        System.out.print("Testing strip\n");
        System.out.print(str.strip());
        System.out.println("\nsuccess");
        
        System.out.print("Testing stripleading\n");
        System.out.print(str.stripLeading());
        System.out.println("\nsuccess");
        
        System.out.print("Testing stripTrailing\n");
        System.out.print(str.stripTrailing());
        System.out.println("\nsuccess");
        System.out.println("====================================================");
        testStringFeatures.testingRepeat();
	}
	
	//The repeat method simply repeats the string that many numbers of times as mentioned in the method in the form of an int.
	public void testingRepeat() {
		String str = "Test OK  ".repeat(3);
        System.out.println(str);
	}	
}
class TestPatternRecognizingFeatures{
	
	//This method is similar to Java 8 method asPredicate(). Introduced in JDK 11, this method will create a predicate if pattern matches with input string.
	public void testingAsMatchPredicate() {
		var str = Pattern.compile("aba").asMatchPredicate();
		String insideValue = "legato";
		System.out.println(str.test(insideValue));
		System.out.println(str.test("anthem"));
		System.out.println("====================================================");
	}
}
class TestTimeUnitFeatures {
	//This method is used to convert the given time to a unit like DAY, MONTH, YEAR and for time too.
	public void testingConvert() {
		TimeUnit timeUnit = TimeUnit.DAYS;
		System.out.println(timeUnit.convert(Duration.ofHours(24)));
		System.out.println(timeUnit.convert(Duration.ofHours(50)));
		System.out.println("====================================================");
	}
}

class TestOptionalFeatures {
	 //Optional.isEmpty() This method returns true if the value of any object is null and else returns false.
	public void testingIsEmpty() {
		Optional<Object> inputOne = Optional.empty();
		System.out.println(inputOne.isEmpty());
		Optional<String> inputTwo = Optional.of("TonyStark");
		System.out.println(inputTwo.isEmpty());
		//Streamsfeatures // takeWhile accept a predicate to determine which elements to abandon from the stream
		Stream.of(1, 2, 3, 2, 1)
	    .takeWhile(n -> n < 3)
	    .collect(Collectors.toList());  // [1, 2]
		System.out.println("====================================================");
	}
}

class TestNestedClassFeatures{
    private String name = "I can able to access private mem in nested class without creation of bridge internally";

     public class A {
        public void printName() {
            System.out.println("RESULT= "+name);            
        }
    }
}
class TestCollectionApiFeatures{
	//we can convert an collection to array .
	public void testingIsArray() {
		ArrayList<Integer> arrayList = new ArrayList<>();
		arrayList.add(10);
		arrayList.add(20);
		arrayList.add(30);
		Integer[] outputArray = arrayList.toArray(value -> new Integer[value]);
		System.out.println(Arrays.toString(outputArray));
		System.out.println(((Object)(outputArray)).getClass().isArray());
		System.out.println("Success converted an collection to Array\n");
		var list = List.of("A", "B", "C"); // List.of()= created a new immutable list from the given arguments. 
		var copy = List.copyOf(list); // creates an immutable copy of the list.
		System.out.println(list == copy);   // true
		var map = Map.of("A", 1, "B", 2); //When creating immutable maps you don't have to create map entries yourself but instead pass keys and values as alternating arguments
		System.out.println(map);    // {B=2, A=1}
		
		System.out.println("\n====================================================");
	}	
}
//class TestHttpClientFeature {
//	public void testingHttpClient()  {
//		System.out.println("Starting sync req...");
//		var request = HttpRequest.newBuilder()
//			    .uri(URI.create("https://api.covid19api.com/summary"))
//			    .GET()
//			    .build();
//			var client = HttpClient.newHttpClient();
//			HttpResponse<String> response;
//			try {				
//				response = client.send(request, HttpResponse.BodyHandlers.ofString());
//				System.out.println(response.body().toString());
//			} catch (IOException | InterruptedException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}	
//			System.out.println("Get method api call is success\n");
//			System.out.println("Starting async req....\n");
//			var requestTwo = HttpRequest.newBuilder()
//				    .uri(URI.create("https://api.covid19api.com/dayone/country/south-africa/status/confirmed"))
//				    .build();
//				var clientTwo = HttpClient.newHttpClient();
//				clientTwo.sendAsync(requestTwo, HttpResponse.BodyHandlers.ofString())
//				    .thenApply(HttpResponse::body)
//				    .thenAccept(System.out::println);
//				System.out.println("printing this line before i get data from sync req");
//			
//			
//			var requestOne = HttpRequest.newBuilder()
//				    .uri(URI.create("https://postman-echo.com/post"))
//				    .header("Content-Type", "text/plain")
//				    .POST(HttpRequest.BodyPublishers.ofString("Hi there!"))
//				    .build();
//				var clientOne = HttpClient.newHttpClient();
//				
//				try {
//					var responseOne = clientOne.send(requestOne, HttpResponse.BodyHandlers.ofString());
//					System.out.println("STATUS CODE --> "+responseOne.statusCode());
//					System.out.println("Post api req success");
//				} catch (IOException | InterruptedException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//			      // 200
//			System.out.println("====================================================");
//	}
//}


